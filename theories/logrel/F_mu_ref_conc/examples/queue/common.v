From Coq.Lists Require Import List.
From iris.algebra Require Import csum excl auth list gmap.
From iris.program_logic Require Import adequacy ectxi_language.

From iris_examples.logrel.F_mu_ref_conc Require Import soundness_binary.
From iris_examples.logrel.F_mu_ref_conc.examples Require Import lock.
From iris.proofmode Require Import tactics.

(* Repeadedly introduce existential variables. *)
Ltac iExistsN := repeat iExists _.

(* Solve a goal that consists of introducing existentials, framing, and picking branches. *)
Ltac iExistsFrame :=
  (repeat iExistsN || iFrame);
  (done || (iLeft; iExistsFrame) || (iRight; iExistsFrame) || fail "Could not solve goal").

Notation "◓ v" := ((1/2)%Qp, to_agree v) (at level 20).
Notation "◔ v" := ((1/4)%Qp, to_agree v) (at level 20).

Section common.
  Definition locO := leibnizO loc.

  Definition fracAgreeR : cmraT := prodR fracR (agreeR locO).

  Context `{heapIG Σ, cfgSG Σ, inG Σ fracAgreeR, inG Σ exlTokR, inG Σ nodeUR, inG Σ nodeStateR}.

  Lemma fracAgree_agree γ (q1 q2 : Qp) v1 v2 :
    own γ (q1, to_agree v1) -∗ own γ (q2, to_agree v2) -∗ ⌜v1 = v2⌝.
  Proof.
    iIntros. by iDestruct (own_valid_2 with "[$] [$]") as %[_ Hv%agree_op_invL'].
  Qed.

  Lemma fracAgree_combine γ (q1 q2 : Qp) v1 v2 :
    own γ (q1, to_agree v1)
    -∗ own γ (q2, to_agree v2)
    -∗ own γ ((q1 + q2)%Qp, to_agree v2) ∗ ⌜v1 = v2⌝.
  Proof.
    iIntros "Hl1 Hl2". 
    iDestruct (fracAgree_agree with "Hl1 Hl2") as %->.
    iCombine "Hl1 Hl2" as "Hl".
    iDestruct (own_valid with "Hl") as %Hv%pair_valid.
    eauto with iFrame.
  Qed.

  Lemma fracAgree_update γ v w u :
    own γ (◓ v) -∗ own γ (◓ w) ==∗ own γ (1%Qp, to_agree u).
  Proof.
    iIntros "Hb1 Hb2".
    iDestruct (fracAgree_combine with "Hb1 Hb2") as "[tok <-]".
    rewrite Qp_half_half.
    iApply (own_update with "tok").
    by apply cmra_update_exclusive.
  Qed.

  Lemma fracAgree_update_one γ v u :
    own γ (1%Qp, to_agree v) ==∗ own γ (1%Qp, to_agree u).
  Proof.
    iIntros. iApply (own_update with "[$]"). by apply cmra_update_exclusive.
  Qed.

  Lemma mapsto_full_to_frac l v : l ↦ᵢ v -∗ l ↦ᵢ{-} v.
  Proof. iIntros. by iExists _. Qed.

  Lemma mapsto_full_to_frac_2 l v : l ↦ᵢ v -∗ l ↦ᵢ{-} v ∗ l ↦ᵢ{-} v.
  Proof.
    iIntros. iDestruct (mapsto_full_to_frac with "[$]") as "H".
    iDestruct (mapsto_frac_duplicable with "H") as "[H1 H2]".
    iFrame.
  Qed.

  Lemma mapsto_full_to_frac_3 l v : l ↦ᵢ v -∗ (l ↦ᵢ{-} v ∗ l ↦ᵢ{-} v ∗ l ↦ᵢ{-} v).
  Proof.
    iIntros.
    iDestruct (mapsto_full_to_frac_2 with "[$]") as "[H1 H2]".
    iDestruct (mapsto_frac_duplicable with "H2") as "[H2 H3]".
    iFrame.
  Qed.

  Lemma mapsto_agree_frac l q v1 v2 : l ↦ᵢ{q} v1 -∗ l ↦ᵢ{-} v2 -∗ ⌜v1 = v2⌝.
  Proof.
    iIntros "P1". iDestruct 1 as (q2) "P2". iApply (mapsto_agree with "P1 P2").
  Qed.

  Lemma mapsto_agree_frac_frac l v1 v2 : l ↦ᵢ{-} v1 -∗ l ↦ᵢ{-} v2 -∗ ⌜v1 = v2⌝.
  Proof.
    iDestruct 1 as (q1) "P1". iDestruct 1 as (q2) "P2".
    iApply (mapsto_agree with "P1 P2").
  Qed.

  (* Maybe commit this upstream. *)
  Lemma mapsto_exclusive l v1 v2 q : l ↦ᵢ v1 -∗ l ↦ᵢ{q} v2 -∗ False.
    iIntros "Hl1 Hl2". iDestruct (mapsto_valid_2 with "Hl1 Hl2") as %[]%Qp_not_plus_q_ge_1.
  Qed.

  Lemma mapsto_exclusive_frac l v1 v2 : l ↦ᵢ v1 -∗ l ↦ᵢ{-} v2 -∗ False.
    iIntros "Hl1 Hl2". iDestruct "Hl2" as (q) "Hl2".
     iDestruct (mapsto_valid_2 with "Hl1 Hl2") as %[]%Qp_not_plus_q_ge_1.
  Qed.

End common.