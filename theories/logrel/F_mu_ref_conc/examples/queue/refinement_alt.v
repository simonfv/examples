From Coq.Lists Require Import List.
From iris.algebra Require Import csum excl auth list gmap.
From iris.program_logic Require Import adequacy ectxi_language.

From iris_examples.logrel.F_mu_ref_conc Require Import soundness_binary.
From iris_examples.logrel.F_mu_ref_conc.examples Require Import lock.
From iris_examples.logrel.F_mu_ref_conc.examples.queue Require Import
  common CG_queue MS_queue_alt.
From iris.proofmode Require Import tactics.

Definition queueN : namespace := nroot .@ "queue".
Definition tailN : namespace := nroot .@ "tail".
Definition nodesN : namespace := nroot .@ "nodes".

Definition fracAgreeR : cmraT := prodR fracR (agreeR locO).

Definition nodeStateR : cmraT := authUR mnatUR.

Definition nodeLive := 0 : mnat.
Definition nodeSentinel := 1 : mnat.
Definition nodeDead := 2 : mnat.

(* States for nodes relationship to tail. *)
Definition notYetLast := 0 : mnat.
Definition isLast := 1 : mnat.
Definition noLongerLast := 2 : mnat.

Canonical Structure gnameO := leibnizO gname.

Definition mapUR : ucmraT := gmapUR loc (agreeR (leibnizO (gname * gname * loc))).
Definition nodeUR : ucmraT := authUR (gmapUR loc (agreeR (leibnizO (gname * gname * loc)))).

Section Queue_refinement.
  Context `{heapIG Σ, cfgSG Σ, inG Σ fracAgreeR, inG Σ nodeUR, inG Σ nodeStateR}.

  Notation D := (prodO valO valO -n> iPropO Σ).

  (* gname naming conventions:
     - γ for the fractional agreement used at the tail.
     - κ for the authorative finite map used in nodesInv.
     - ι for the authorative sum for the state of nodes in relation to the head.
     - ω for the authorative sum for the state of nodes in relation to the tail.
   *)

  Definition noneV := InjLV UnitV.
  Definition someV v := InjRV v.

  Lemma node_update ι (x y : mnat) : x ≤ y → own ι (● x) ==∗ own ι (● y).
  Proof.
    iIntros. iApply (own_update with "[$]"). by eapply auth_update_auth, mnat_local_update.
  Qed.

  Lemma update_sentinel_dead ι : own ι (● nodeSentinel) ==∗ own ι (● nodeDead).
  Proof. iApply node_update. unfold nodeSentinel, nodeDead. lia. Qed.

  Lemma update_live_sentinel ι : own ι (● nodeLive) ==∗ own ι (● nodeSentinel).
  Proof. iApply node_update. unfold nodeLive, nodeSentinel. lia. Qed.

  Lemma update_yet_is ω : own ω (● notYetLast) ==∗ own ω (● isLast).
  Proof. iApply node_update. unfold notYetLast, isLast. lia. Qed.

  Lemma update_last_no_longer ω : own ω (● isLast) ==∗ own ω (● noLongerLast).
  Proof. iApply node_update. unfold isLast, noLongerLast. lia. Qed.

  Lemma state_agree ι q p (s1 s2 : mnat) : own ι (●{q} s1) -∗ own ι (●{p} s2) -∗ ⌜s1 = s2⌝.
  Proof.
    iIntros. by iDestruct (own_valid_2 with "[$] [$]") as %E%auth_auth_frac_op_invL.
  Qed.

  Lemma state_leq ι q (s1 s2 : mnat) : own ι (●{q} s1) -∗ own ι (◯ s2) -∗ ⌜s2 ≤ s1⌝.
  Proof.
    iIntros. by iDestruct (own_valid_2 with "[$] [$]") as %[_ [a%mnat_included _]]%auth_both_frac_valid.
  Qed.

  Definition node_singleton ℓs ι ω ℓn : mapUR := {[ ℓs := to_agree ((ι, ω, ℓn) : leibnizO (gname * gname * loc))]}.

  Definition node_mapsto κ ℓs (ι ω : gname) (ℓn : loc) : iProp Σ :=
    own κ (◯ {[ ℓs := to_agree ((ι, ω, ℓn) : leibnizO (gname * gname * loc))]} : nodeUR).

  (* Represents the information that the location ℓ points to a series of nodes
     correscponding to the list `xs`.
   *)
  Fixpoint isNodeList γ κ (ℓ : loc) (xs : list val) : iProp Σ :=
    match xs with
    | nil => ∃ ℓ2, ℓ ↦ᵢ{1/2} (LocV ℓ2) ∗ ℓ2 ↦ᵢ{-} FoldV noneV ∗ own γ (◓ ℓ)
    | x :: xs' =>
      (∃ ℓ2 ℓnext ι ω,
          own κ (◯ {[ ℓ2 := to_agree (ι, ω, ℓnext) ]})
        ∗ own ι (●{1/2} nodeLive)
        ∗ ℓ ↦ᵢ{-} (LocV ℓ2) ∗ ℓ2 ↦ᵢ{-} FoldV (someV (PairV (InjRV x) (LocV ℓnext)))
        ∗ isNodeList γ κ ℓnext xs')
    end.

  Definition nextNode γ κ ℓinto : iProp Σ :=
    ∃ ℓn,
      ((* node is nil *) 
        ℓinto ↦ᵢ{1/2} (LocV ℓn) ∗ (* We own half the pointer into the node. *)
        ℓn ↦ᵢ{-} (FoldV noneV) ∗
        own γ (◓ ℓinto)) (* Proof that ℓp is the actual pointer to nil in the queue. *)
      ∨ ((* non-nil *)
        ∃ ιnext ωnext ℓnext,
          ℓinto ↦ᵢ{-} LocV ℓn ∗
          node_mapsto κ ℓn ιnext ωnext ℓnext
      ).

  (*
    ℓq is the queue pointer.
    ℓn is the "start" pointer of the node.
  *)
  Definition nodeInv (γ κ ι ω : gname) (ℓq ℓt ℓn ℓtoNext : loc) : iProp Σ :=
    ∃ x,
      ℓn ↦ᵢ{-} FoldV (someV (PairV x (LocV ℓtoNext))) ∗
        nextNode γ κ ℓtoNext ∗
      ((* ℓ is dead. It has been the sentinel, but no longer is. *)
        (own ι (● nodeDead) ∗
        (* We know that the node points to a non-nil node. *)
        ∃ x' b ℓnext, ℓtoNext ↦ᵢ{-} LocV ℓnext ∗ ℓnext ↦ᵢ{-} FoldV (someV (PairV x' b)))
      ∨ (* ℓn is currently the sentinel. *)
        (own ι (●{1/2} nodeSentinel) ∗
        ℓq ↦ᵢ{1/2} (LocV ℓn)) (* We own half the pointer into the sentinel. *)
      ∨ (* The node is part of the logical queue. *)
        (own ι (●{1/2} nodeLive))) ∗
      (* Tail state *)
      ((* We know that the node points to a non-nil node. *)
        (own ω (● noLongerLast) ∗
        ∃ x' b ℓnext, ℓtoNext ↦ᵢ{-} LocV ℓnext ∗ ℓnext ↦ᵢ{-} FoldV (someV (PairV x' b)))
        ∨
        (own ω (● isLast) ∗ ℓt ↦ᵢ{1/2} (LocV ℓn))
        ∨
        (own ω (●{1/2} notYetLast))).

  (* Predicate expressing that ℓq points to a queue with the values xs *)
  Definition isMSQueue γ κ (τi : D) (ℓq ℓt : loc) (xsᵢ : list val) : iProp Σ :=
    (∃ ℓsentinel ℓlast ℓhdPt ℓpt ι ω ι' ω',
        ℓq ↦ᵢ{1/2} (LocV ℓsentinel) (* queue own half the pointer, the sentinels owns the other half. *)
        ∗ node_mapsto κ ℓsentinel ι ω ℓhdPt
        ∗ ℓt ↦ᵢ{1/2} (LocV ℓlast) (* queue own half the pointer, the last owns the other half. *)
        ∗ node_mapsto κ ℓlast ι' ω' ℓpt
        ∗ own ι (●{1/2} nodeSentinel)
        ∗ own ω' (◯ isLast)
        ∗ isNodeList γ κ ℓhdPt xsᵢ).

  (* Expresses that ℓ points to a non-nil node. *)
  Definition pointsToSome ℓ : iProp Σ :=
    ∃ ℓn x next, ℓ ↦ᵢ{-} (LocV ℓn) ∗ ℓn ↦ᵢ{-} FoldV (someV (PairV x next)).

 (* Ties the map to nodeInv  *)
  Definition map_map γ κ ℓq ℓt (m : gmap loc (gname * gname * loc)) : iProp Σ :=
    ([∗ map] ℓs ↦ a ∈ m, (∃ ι ω ℓn, ⌜a = (ι, ω, ℓn)⌝ ∗ nodeInv γ κ ι ω ℓq ℓt ℓs ℓn))%I.

  Definition nodesInv γ κ ℓq ℓt : iProp Σ :=
    ∃ (m : gmap loc (gname * gname * loc)),
      own κ (● (to_agree <$> m) : nodeUR) ∗
      map_map γ κ ℓq ℓt m.

  Lemma mapUR_alloc (m : mapUR) (i : loc) v :
    m !! i = None → ● m ~~> ● (<[i := to_agree v]> m) ⋅ ◯ {[ i := to_agree v ]}.
  Proof. intros. by apply auth_update_alloc, alloc_singleton_local_update. Qed.

  Lemma insert_node_subset γ κ ℓq ℓt ℓs ι ω ℓn m m' :
    ⌜m' ⊆ m⌝ -∗
    ⌜m !! ℓs = None⌝ -∗
    own κ (● (to_agree <$> m) : nodeUR) -∗
    ▷ (map_map γ κ ℓq ℓt m') -∗
    nodeInv γ κ ι ω ℓq ℓt ℓs ℓn
    ==∗
    own κ (● (to_agree <$> (<[ℓs := (ι, ω, ℓn)]> m)) : nodeUR) ∗
    ▷ (map_map γ κ ℓq ℓt (<[ℓs := (ι, ω, ℓn)]> m')) ∗
    node_mapsto κ ℓs ι ω ℓn.
  Proof.
    iIntros (sub non) "auth mon sentInv".
    iMod (own_update with "auth") as "[auth frag]".
    { apply (mapUR_alloc _ ℓs). rewrite lookup_fmap. rewrite non. done. }
    rewrite fmap_insert.
    iFrame.
    iApply big_sepM_insert. { by eapply lookup_weaken_None. }
    iModIntro.
    iExistsFrame.
  Qed.

  Lemma insert_node γ κ ω ℓq ℓt ℓs ι ℓn m :
    ⌜m !! ℓs = None⌝ -∗
    own κ (● (to_agree <$> m) : nodeUR) -∗
    map_map γ κ ℓq ℓt m -∗
    nodeInv γ κ ι ω ℓq ℓt ℓs ℓn
    ==∗
    own κ (● (to_agree <$> (<[ℓs := (ι, ω, ℓn)]> m)) : nodeUR) ∗
    map_map γ κ ℓq ℓt (<[ℓs := (ι, ω, ℓn)]> m) ∗
    node_mapsto κ ℓs ι ω ℓn.
  Proof.
    iIntros "% auth mon sentInv".
    iMod (own_update with "auth") as "[auth frag]".
    { apply (mapUR_alloc _ ℓs). rewrite lookup_fmap. rewrite a. done. }
    rewrite fmap_insert.
    iFrame.
    iApply big_sepM_insert; first done.
    iExistsFrame.
  Qed.

  Lemma map_singleton_included (m : gmap loc (gname * gname * loc)) (l : loc) v :
    ({[l := to_agree v]} : mapUR) ≼ ((to_agree <$> m) : mapUR) → m !! l = Some v.
  Proof.
    move /singleton_included_l=> -[y].
    rewrite lookup_fmap fmap_Some_equiv => -[[x [-> ->]]].
    by move /Some_included_total /to_agree_included /leibniz_equiv_iff ->.
  Qed.

  Lemma auth_node_mapsto_Some γ m ℓs ι ω ℓn :
    own γ (● (to_agree <$> m) : nodeUR) -∗
    node_mapsto γ ℓs ι ω ℓn -∗
    ⌜m !! ℓs = Some (ι, ω, ℓn)⌝.
  Proof.
    iIntros. by iDestruct (own_valid_2 with "[$] [$]") as %[E%map_singleton_included _]%auth_both_valid.
  Qed.

  Lemma node_mapsto_agree γ ℓs ι ω ι' ω' ℓn ℓn' :
    node_mapsto γ ℓs ι ω ℓn -∗ node_mapsto γ ℓs ι' ω' ℓn' -∗ ⌜ι = ι'⌝ ∗ ⌜ω = ω'⌝ ∗ ⌜ℓn = ℓn'⌝.
  Proof.
    iIntros "a b".
    unfold node_mapsto.
    iDestruct (own_valid_2 with "a b") as %Hv.
    rewrite -auth_frag_op in Hv.
    apply (auth_frag_valid (_ ⋅ _)) in Hv. (* Why is this necessary? *)
    rewrite singleton_op in Hv.
    apply singleton_valid, agree_op_invL' in Hv.
    inversion Hv.
    done.
  Qed.

  (* Reinsert a node that has been taken out. *)
  Lemma reinsert_node_Some γ κ ω ℓq ℓt ℓs ι ℓn (m : gmap loc (gname * gname * loc)) :
    m !! ℓs = Some (ι, ω, ℓn) →
    map_map γ κ ℓq ℓt (delete ℓs m) -∗
    nodeInv γ κ ι ω ℓq ℓt ℓs ℓn
    -∗
    map_map γ κ ℓq ℓt m.
  Proof.
    iIntros (eq) "mon new".
    iDestruct (big_sepM_insert_delete with "[new $mon]") as "mon".
    { iExistsFrame. }
    rewrite insert_id; done.
  Qed.

  (* Reinsert a node that has been taken out. *)
  Lemma reinsert_node γ κ ω ℓq ℓt ℓs ι ℓn (m : gmap loc (gname * gname * loc)) :
    own κ (● (to_agree <$> m) : nodeUR) -∗
    node_mapsto κ ℓs ι ω ℓn -∗
    map_map γ κ ℓq ℓt (delete ℓs m) -∗
    nodeInv γ κ ι ω ℓq ℓt ℓs ℓn
    -∗
    own κ (● (to_agree <$> m) : nodeUR) ∗
    map_map γ κ ℓq ℓt m.
  Proof.
    iIntros "ownM frag mon new".
    iDestruct (own_valid_2 with "ownM frag") as %[inc _]%auth_both_valid.
    iFrame.
    iApply (reinsert_node_Some with "mon"); first by apply map_singleton_included.
    iFrame.
  Qed.

  Lemma node_lookup (γ κ ι ω : gname) (ℓq ℓt ℓs ℓn : loc) (m : gmap loc (gname * gname * loc)) :
    own κ (● (to_agree <$> m) : nodeUR) -∗
    ▷ map_map γ κ ℓq ℓt m -∗
    node_mapsto κ ℓs ι ω ℓn -∗
    ▷ nodeInv γ κ ι ω ℓq ℓt ℓs ℓn ∗
    own κ (● (to_agree <$> m) : nodeUR) ∗
    ▷ map_map γ κ ℓq ℓt (delete ℓs m).
  Proof.
    iIntros "ownM mon frag".
    iDestruct (auth_node_mapsto_Some with "ownM frag") as %S.
    iDestruct (big_sepM_later with "mon") as "mon".
    iDestruct (big_sepM_delete with "mon") as "[singl mon]"; first done.
    iDestruct "singl" as (? ? ?) "[eq sentInv]".
    iDestruct (big_sepM_later with "mon") as "mon".
    iFrame.
    iNext.
    iDestruct "eq" as %[= <- <- <-].
    iFrame.
  Qed.

  Definition queueInv γ κ τi ℓq ℓt ℓs ℓlock: iProp Σ :=
    (∃ xsᵢ xsₛ,
        isMSQueue γ κ τi ℓq ℓt xsᵢ
      ∗ isCGQueue ℓs xsₛ
      ∗ ℓlock ↦ₛ (#♭v false)
      ∗ [∗ list] xᵢ ; xₛ ∈ xsᵢ ; xsₛ, τi (xᵢ, xₛ)
    )%I.

  (* With the token and the nodeList one can perform a CAS on the last node. *)
  Lemma enqueue_cas E γ κ xs x (m : gmap loc (gname * gname * loc)) (ℓsentinel ℓq ℓt ℓ ℓ2 ℓhdPt ℓnil ℓtail ℓnode : loc) :
    {{{
      ⌜ℓsentinel ≠ ℓnode⌝ ∗
      own κ (● (to_agree <$> m) : nodeUR) ∗
      ▷ map_map γ κ ℓq ℓt (delete ℓsentinel m) ∗
      inv nodesN (nodesInv γ κ ℓq ℓt) ∗
      ℓnil ↦ᵢ FoldV (InjLV UnitV) ∗
      ℓtail ↦ᵢ (LocV ℓnil) ∗ (* ℓtail points to the nil node *)
      ℓnode ↦ᵢ (FoldV (InjRV (PairV (InjRV x) (LocV ℓtail)))) ∗ (* node contains x and points to nil *)
      ▷ isNodeList γ κ ℓhdPt xs ∗
      ℓ ↦ᵢ{1/2} (LocV ℓ2) ∗
      own γ (◓ ℓ) (* Proof that ℓ is the pointer pointing to nil. *)
    }}}
    CAS (Loc ℓ) (Loc ℓ2) (Loc ℓnode) @ E
    {{{ RET (BoolV true);
      ∃ (ι ω : gname),
      own κ (● (to_agree <$> (<[ℓnode := (ι, ω, ℓtail)]> m)) : nodeUR) ∗
      ▷ map_map γ κ ℓq ℓt (delete ℓsentinel (<[ℓnode := (ι, ω, ℓtail)]>m)) ∗
      ℓnode ↦ᵢ{-} (FoldV (InjRV (PairV (InjRV x) (LocV ℓtail)))) ∗
      isNodeList γ κ ℓhdPt (xs ++ [x]) ∗
      node_mapsto κ ℓnode ι ω ℓtail ∗
      ℓ ↦ᵢ{-} (LocV ℓnode) ∗
      own ω (●{1 / 2} notYetLast)
    }}}.
  Proof.
    iIntros (ϕ) "(% & authM & bigSep & #nodesInv & nilPts & tailPts & nodePts & nodeList & ℓPts & tok) Hϕ".
    iInduction xs as [|x' xs'] "IH" forall (ℓhdPt).
    - iDestruct (mapsto_full_to_frac_2 with "nilPts") as "(nilPts & nilPts')".
      iDestruct "tailPts" as "[tailPts tailPts']".
      iDestruct "nodeList" as (ℓ0) ">(ℓhdPt & _ &tok')".
      (* We need to be able to conclude that ℓhdPt is equal to ℓ. *)
      iDestruct (fracAgree_agree with "tok tok'") as %<-.
      iDestruct (mapsto_combine with "ℓhdPt ℓPts") as "[ℓpts %]".
      inversion_clear H5.
      rewrite Qp_half_half.
      iMod (fracAgree_update with "tok tok'") as "[tok tok']".
      iMod (own_alloc (● nodeLive ⋅ ◯ _)) as (ι) "[[authNodeState authNodeState'] _]".
      { by apply auth_both_valid. }
      iMod (own_alloc (● notYetLast ⋅ ◯ _)) as (ω) "[[lastState lastState'] _]".
      { by apply auth_both_valid. }
      destruct (m !! ℓnode) as [v|] eqn:Eq.
      { iDestruct (big_sepM_lookup with "bigSep") as "conj".
        { rewrite lookup_delete_ne; done. }
        iDestruct "conj" as (ι0 ω0 ℓn) "(>-> & sentInv)".
        iDestruct "sentInv" as (v') "(>nodePts' & _)".
        iDestruct (mapsto_exclusive_frac with "nodePts nodePts'") as %[]. }
      iDestruct (mapsto_full_to_frac_3 with "nodePts") as "(nodePts & nodePts' & nodePts'')".
      iMod (insert_node_subset _ _ _ _ _ _ _ _ m (delete ℓsentinel m) with "[%] [% //] authM bigSep [authNodeState' nodePts'' tok tailPts' nilPts' lastState']")
         as "(authM & bigSep & #frag)".
      { apply delete_subseteq. }
      { iExists _. iFrame "nodePts''".
        iSplitL "tailPts' nilPts' tok". iExistsFrame.
        iSplitR "lastState'". iRight. iRight. iFrame.
        iRight. iRight. iFrame. }
      iApply (wp_cas_suc with "ℓpts"). iNext. iIntros "ℓpts".
      iDestruct (mapsto_full_to_frac_2 with "ℓpts") as "(ℓpts & ℓpts')".
      iApply "Hϕ". iExistsN.
      rewrite delete_insert_ne; last done.
      iFrame "frag nodePts". iFrame.
      simpl.
      iExistsN. iFrame "frag nodePts'". iFrame.
      iFrame. iExists _. iFrame "tailPts". iFrame.
    - iDestruct "nodeList" as (? ? ? ?) "(? & ? & ? & ? & nodeListTail)".
      iApply ("IH" with "authM bigSep nilPts tailPts nodePts nodeListTail ℓPts tok").
      iNext.
      iDestruct 1 as (? ?) "(authM & bigSep & nilPts & tailPts & nodePts & ℓPts)".
      iApply "Hϕ".
      iExistsFrame.
  Qed.

  (* This lemma has been commited upstream to Iris and will be available in the future. *)
  Lemma auth_update_core_id_frac {A : ucmraT} (a b : A) `{!CoreId b} q :
    b ≼ a → ●{q} a ~~> ●{q} a ⋅ ◯ b.
  Proof. Admitted.

  Lemma load_tail γ κ τi ℓq ℓt list ℓlock :
   {{{ inv queueN (queueInv γ κ τi ℓq ℓt list ℓlock) }}}
     Load (Loc ℓt)
   {{{ ι ω ℓlast ℓpt, RET (LocV ℓlast); node_mapsto κ ℓlast ι ω ℓpt ∗ own ω (◯ isLast) }}}.
  Proof.
    iIntros (ϕ) "inv Hϕ".
    iInv queueN as (xs xsₛ) "(isMSQ & Hrest)" "Hclose".
    iDestruct "isMSQ" as (? ? ? ? ? ? ? ?) "(? & ? & >tPts & >#lastMapsto & ? & #tailState & ?)".
    iApply (wp_load with "tPts"). iNext. iIntros "tPts". 
    iMod ("Hclose" with "[-Hϕ]"). { iNext. iExistsN. iFrame. iExistsN. iFrame "tailState". by iFrame. }
    iModIntro. iApply "Hϕ". auto.
  Qed.

  Lemma load_sentinel γ κ τi ℓq ℓt list ℓlock :
   {{{ inv queueN (queueInv γ κ τi ℓq ℓt list ℓlock) }}}
     Load (Loc ℓq)
   {{{ ℓsentinel ι ω ℓhdPt, RET (LocV ℓsentinel);
       node_mapsto κ ℓsentinel ι ω ℓhdPt ∗ own ι (◯ nodeSentinel) }}}.
  Proof.
    iIntros (ϕ) "inv Hϕ".
    iInv queueN as (xs xsₛ) "(isMSQ & Hrest)" "Hclose".
    iDestruct "isMSQ" as (? ? ? ? ι ω ι' ω') "(>qPts & >#frag & ? & ? & >nodeState & ? & ?)".
    iMod (own_update with "nodeState") as "[authNodeState nodeState]".
    { by apply (auth_update_core_id_frac _ nodeSentinel). }
    iApply (wp_load with "qPts"). iNext. iIntros "qPts".
    iMod ("Hclose" with "[-Hϕ nodeState]"). { iNext. iExistsFrame. }
    iModIntro. iApply "Hϕ". iFrame. auto.
  Qed.

  Lemma MS_CG_counter_refinement :
    [] ⊨ MS_queue ≤log≤ CG_queue :
      (TForall (TProd
                  (TArrow TUnit (TMaybe (TVar 0)))
                (TArrow (TVar 0) TUnit)
               )
      ).
  Proof.
    iIntros (Δ vs ?) "#[Hspec HΓ]".
    iDestruct (interp_env_empty with "HΓ") as "->". iClear "HΓ".
    iIntros (j K) "Hj".
    iApply wp_value.
    iExists (TLamV _). iFrame "Hj". clear j K.
    iAlways. iIntros (τi) "%". iIntros (j K) "Hj /=".
    iMod (do_step_pure with "[$Hj]") as "Hj"; auto.
    iMod (steps_newlock _ j (LetInCtx _ :: K) with "[$Hj]")
      as (ℓlock) "[Hj lockPts]"; auto.
    iMod (do_step_pure _ j K with "[$Hj]") as "Hj"; auto.
    iApply wp_pure_step_later; auto. iNext.

    (* Stepping through the specs allocation *)
    iMod (step_alloc  _ j (LetInCtx _ :: K) with "[$Hj]") as (list) "[Hj listPts']"; auto.
    simpl.
    iMod (do_step_pure _ j K with "[$Hj]") as "Hj"; auto.

    (* Stepping through the initialization of the sentinel *)
    iApply (wp_bind (fill [LetInCtx _])).
    iApply (wp_bind (fill [AllocCtx])).
    (* iApply (wp_bind (fill [AllocCtx])). *)
    iApply (wp_bind (fill [PairRCtx (InjLV UnitV); InjRCtx; FoldCtx])).
    iApply (wp_bind (fill [AllocCtx])).
    iApply wp_alloc; first done.
    iNext. iIntros (nil) "[nilPts nilPts'] /=".
    iApply wp_alloc; first done.
    iNext. iIntros (tail) "[tailPts1 tailPts2] /=".
    iApply wp_value.
    iApply wp_alloc; first done. iNext.
    iIntros (ℓsentinel) "sentinelPts /=".
    iDestruct (mapsto_full_to_frac_2 with "sentinelPts") as "(sentinelPts & sentinelPts')".
    iApply wp_pure_step_later; auto. iNext.
    iApply (wp_bind (fill [LetInCtx _])). iAsimpl.
    iApply wp_alloc; first done. iNext.
    iIntros (ℓt) "[tPts tPts'] /=".
    iApply wp_pure_step_later; auto. iNext.
    iApply (wp_bind (fill [LetInCtx _])). iAsimpl.
    iApply wp_alloc; first done. iNext.
    iIntros (ℓq) "[qPts qPts'] /=".
    iApply wp_pure_step_later; auto. iNext.

    (* Allocate the nodes invariant. *)
    iMod (own_alloc (● nodeSentinel)) as (ι) "[authNodeSentinel authNodeSentinel']".
    { by apply auth_auth_valid. }
    iMod (own_alloc (● isLast ⋅ ◯ isLast))
      as (ω) "[authLast fragLast]".
    { by apply auth_both_valid. }
    iMod (own_alloc (● (node_singleton ℓsentinel ι ω tail) ⋅ ◯ (node_singleton ℓsentinel ι ω tail))) as (κ) "ownO".
    { apply auth_both_valid. split; first done. apply singleton_valid. done. }
    iDestruct "ownO" as "[authO #fragO]".
    (* We now want to allocate the invariant for the queue. To do that we first
       establish the node invariant for the empty node we have created. *)
    iMod (own_alloc (1%Qp, to_agree tail)) as (γ) "[token1 token2]"; first done.
    iMod (inv_alloc nodesN _ (nodesInv _ _ ℓq ℓt) with "[authO qPts' authNodeSentinel' tPts' authLast sentinelPts' token1 tailPts1 nilPts]") as "#nodesInv".
    { iNext.
      iExists {[ℓsentinel := (ι, ω, tail) : leibnizO (gname * gname * loc)]}.
      iSplitL "authO".
      { rewrite map_fmap_singleton. iAssumption. }
      rewrite /map_map big_sepM_singleton.
      iExistsN. iSplit; auto.
      iExistsN. iFrame "sentinelPts'".
      iSplitL "tailPts1 nilPts token1". { iExistsFrame. }
      iSplitR "authLast tPts'". { iExistsFrame. } { iExistsFrame. } }
    iMod (inv_alloc queueN _ (queueInv _ _ τi ℓq ℓt list _)
            with "[qPts tPts lockPts listPts' sentinelPts tailPts2 nilPts' token2 authNodeSentinel fragO fragLast]")
      as "#Hinv".
    { iNext. iExists [], []. simpl.
      iFrame. iExistsN. iFrame. iFrame "fragO". iExistsFrame. }
    iApply wp_value.
    iAsimpl.
    iExists (PairV (CG_dequeueV _ _) (CG_enqueueV _ _)).
    simpl. rewrite CG_dequeue_to_val CG_enqueue_to_val.
    iFrame.
    iClear "fragO".
    clear j K ι ω ℓsentinel.
    iExists (_, _), (_, _).
    iSplitL; first auto.
    iSplit.
    - (* dequeue *)
      iIntros (vv) "!> [-> ->]".
      iIntros (j K) "Hj". simpl.
      rewrite with_lock_of_val.
      iLöb as "Hlat".
      iApply wp_pure_step_later; auto. iNext. iAsimpl.
      iApply (wp_bind (fill [LetInCtx _])).
      iApply (load_sentinel with "[$]"). iNext.
      iIntros (ℓsentinel ι ? ℓhdPt) "(#fragO & nodeState)".
      simpl.
      iApply wp_pure_step_later; auto. iNext. iAsimpl.
      iApply (wp_bind (fill [LetInCtx _])).
      iApply (wp_bind (fill [UnfoldCtx; CaseCtx _ _])).
      iInv nodesN as (m) "[>authM bigSep]" "closeNodesInv".
      iDestruct (node_lookup with "authM bigSep fragO") as "(sentInv & authM & bigSep)".
      iDestruct "sentInv" as (x) "(>sentinelPts & rest)".
      iDestruct (mapsto_frac_duplicable with "sentinelPts") as "(sentinelPts & sentinelPts')".
      iApply (wp_load_frac with "sentinelPts").
      iNext. iIntros "sentinelPts".
      iDestruct (reinsert_node with "authM fragO bigSep [sentinelPts' rest]") as "(authM & bigSep)".
      { iExistsFrame. }
      iMod ("closeNodesInv" with "[authM bigSep]") as "_".
      { iNext. iExistsFrame. }
      iModIntro.
      clear m.
      iApply (wp_bind (fill [CaseCtx _ _])).
      iApply wp_pure_step_later; auto. iNext.
      iApply wp_value.
      simpl. iAsimpl.
      iApply wp_pure_step_later; auto. iNext. simpl.
      iApply wp_value.
      iApply wp_pure_step_later; auto. iNext.
      iApply (wp_bind (fill [CaseCtx _ _])).
      iAsimpl.
      iApply (wp_bind (fill [UnfoldCtx])).
      iApply (wp_bind (fill [LoadCtx])).
      iApply (wp_bind (fill [LoadCtx])).
      iApply wp_pure_step_later; auto. iNext.
      iApply wp_value. simpl.
      iInv nodesN as (m) "[>authM bigSep]" "closeNodesInv".
      iDestruct (node_lookup with "authM bigSep fragO") as "(sentInv & authM & bigSep)".
      iDestruct "sentInv" as (x') "(sentinelPts' & next & disj & disj2)".
      iDestruct "next" as (ℓn) "[Left | Right]".
      + (* xs is the empty list *)
        iDestruct "Left" as ">(hdPtPts & nPts & tok)".
        iDestruct (mapsto_frac_duplicable with "nPts") as "(nPts & nPts')".
        simpl.
        iDestruct "disj" as "[>[_ pts] | [sent | >live]]".
        1: { 
          iDestruct "pts" as (x1 b ℓnext) "(hdPtPts' & nextPts)".
          iDestruct (mapsto_agree_frac with "hdPtPts hdPtPts'") as %[= ->].
          iDestruct (mapsto_agree_frac_frac with "nPts nextPts") as %[=]. }
        2: { iDestruct (state_leq with "live nodeState") as %le. inversion le. }
        iDestruct "sent" as ">(authState & qPts)".
        iInv queueN as (xs2 xsₛ2) "(isMSQ & >Hsq & >lofal & Hlink)" "closeQueueInv".
        rewrite /isMSQueue.
        iDestruct "isMSQ" as (ℓsentinel2 ℓlast2 ℓhdPt2 ? ? ? ? ?)
          "(>qPts2 & >#fragO' & tPts & tailMapsto & >nodeState' & isLast & nodeList)".
        (* We can conclude that the queue still points to the same sentinel that
           we read previously. *)
        iDestruct (mapsto_agree with "qPts qPts2") as %[= <-].
        iDestruct (node_mapsto_agree with "fragO fragO'") as %[<- [<- <-]].
        iClear "fragO'".
        (* Since the sentinel is the same and the sentinel points to a cons and
           not a nil the queue cannot be empty. *)
        destruct xs2 as [|x2 xs2'].
        2: {
          iDestruct "nodeList" as (ℓ2 next q1 ι0) "(_ & _ & >hdPtPts' & >ℓ2Pts & _)".
          iDestruct (mapsto_agree_frac with "hdPtPts hdPtPts'") as %[= <-].
          iDestruct (mapsto_agree_frac_frac with "nPts ℓ2Pts") as %[=]. }
        destruct xsₛ2 as [|x2' xsₛ2'].
        2: { iDestruct (big_sepL2_length with "Hlink") as ">%". inversion H6. }
        simpl.
        (* We now step over the specification code. *)
        iMod (steps_CG_dequeue_nil with "[$Hspec $Hj $Hsq $lofal]") as "(Hj & Hsq & lofal)".
        { solve_ndisj. }
        iApply (wp_load with "[$hdPtPts]"). iNext. iIntros "hdPtPts".
        (* We now close all the invariants that we opened. *)
        iDestruct (reinsert_node with "authM fragO bigSep [sentinelPts authState qPts2 hdPtPts nPts' tok disj2]") as "(authM & bigSep)".
        { iExistsN. iFrame.
          iSplitL "hdPtPts nPts' tok". { iExistsFrame. }
          iRight. iLeft. iFrame. }
        iMod ("closeQueueInv" with "[qPts tPts lofal Hsq nodeList tailMapsto nodeState' isLast]") as "_".
        { iNext. iExists [], []. simpl.
          iFrame. iExistsN. iFrame. auto. }
        iModIntro.
        iMod ("closeNodesInv" with "[authM bigSep]") as "_".
        { iNext. iExists _. iFrame. }
        iModIntro.
        iApply (wp_load_frac with "nPts"). iNext. iIntros "otherPts".
        iApply wp_pure_step_later; auto. iNext. simpl.
        iApply wp_value. simpl.
        iApply wp_pure_step_later; auto. iNext. simpl.
        iApply wp_value.
        iExists (noneV). iFrame.
        iLeft. iExists (_, _). simpl. done.
      + (* xs is not the empty list *)
        iDestruct "Right" as (ι2 ω2 next) "(>hdPtPts & #nextMapsto)".
        iDestruct (mapsto_frac_duplicable with "hdPtPts") as "(hdPtPts & hdPtPts')".
        iApply (wp_load_frac with "[$hdPtPts]"). iNext. iIntros "hdPtPts".
        simpl.
        iDestruct (reinsert_node with "authM fragO bigSep [sentinelPts' disj hdPtPts' disj2]") as "(authM & bigSep)".
        { iExistsN. iFrame.
          iExists _. iRight. iExists _, _, _. iFrame. auto. }
        iMod ("closeNodesInv" with "[authM bigSep]") as "_".
        { iNext. iExists _. iFrame. }
        iModIntro. clear m.
        iInv nodesN as (m) "[>authM bigSep]" "closeNodesInv".
        iDestruct (node_lookup with "authM bigSep nextMapsto") as "(nextInv & authM & bigSep)".
        iDestruct "nextInv" as (x2) "(otherPts & rest)".
        iDestruct (mapsto_frac_duplicable with "otherPts") as "(otherPts & otherPts')".
        iApply (wp_load_frac with "otherPts"). iNext. iIntros "otherPts".
        simpl.
        iDestruct (reinsert_node with "authM nextMapsto bigSep [otherPts' rest]") as "(authM & bigSep)".
        { iExistsFrame. }
        iMod ("closeNodesInv" with "[authM bigSep]") as "_".
        { iNext. iExists _. iFrame. }
        iModIntro. clear m.
        iApply wp_pure_step_later; auto. iNext.
        iApply wp_value.
        iApply wp_pure_step_later; auto. iNext.
        iApply (wp_bind (fill [IfCtx _ _])).
        iAsimpl.
        iApply (wp_bind (fill [CasRCtx (LocV _) (LocV _)])).
        iApply (wp_bind (fill [LoadCtx])).
        iApply wp_pure_step_later; auto. iNext.
        iApply wp_value. simpl.
        iApply (wp_load_frac with "hdPtPts"). iNext. iIntros "hdPtPts".
        simpl.
        iInv queueN as (xs2 xsₛ2) "(isMSQ & Hsq & lofal & Hlink)" "Hclose".
        rewrite /isMSQueue.
        iDestruct "isMSQ" as (ℓsentinel2 ℓlast2 ℓhdPt2 ι' ? ? ? ?)
          "(>qPts & >#fragO' & tPts & tailMapsto & >sentinelState & lastState & nodeList)".
        (* We now open the nodes invariant. *)
        iInv nodesN as (m) "[>authM bigSep]" "closeNodesInv".
        iDestruct (node_lookup with "authM bigSep fragO'") as "(sentInv & authM & bigSep)".
        iDestruct "sentInv" as (v3)  "(>sentinelPts' & next & [[>ownO _]|[>snd| >ownO]] & disj)";
        try iDestruct (state_agree with "sentinelState ownO") as %[=].
        iDestruct "snd" as "(ownO & qPts2)".
        iCombine "sentinelState ownO" as "sentinelState".
        iDestruct (mapsto_combine with "qPts qPts2") as "[qPts %]".
        rewrite Qp_half_half.
        iDestruct (auth_node_mapsto_Some with "authM fragO'") as %sentSome.
        destruct (decide (ℓsentinel2 = ℓsentinel)) as [|Hneq]; subst.
        * (* The queue still points to the same sentinel that we read earlier--the CAS succeeds *)
          iApply (wp_cas_suc with "qPts"); auto.
          iNext. iIntros "qPts".
          (* We have opened the queue invariant twice. Since the queue pointer
             still points to the same sentinel a lot of the existential variables
             we receieved the first time around are equal to the ones we have now.
             Establishing this is critical. *)
          iDestruct (mapsto_agree_frac_frac with "sentinelPts sentinelPts'") as %[= <- <-].
          (* xs2 is not necessarily equal to xs, but, since the CAS succeeded,
             it still has xs as a prefix. And since we know that xs is a cons xs2
             must also be a cons with the same element. *)
          destruct xs2 as [|x2' xs2']; simpl.
          { iDestruct "nodeList" as (ℓ2) "(ℓhdPts & ℓ2Pts & _)".
            iDestruct (mapsto_agree_frac with "ℓhdPts hdPtPts") as %[= ->].
            iDestruct (mapsto_agree_frac_frac with "otherPts ℓ2Pts") as %[=]. }
          destruct xsₛ2 as [|xₛ2' xsₛ2']; first done.
          iDestruct "nodeList" as (ℓtail ℓnext ι3 ω3) "(#frag & nodeState' & hdPtPts' & tailPts & nodeList)".
          iDestruct (mapsto_agree_frac_frac with "hdPtPts hdPtPts'") as %[= ->].
          iAssert (⌜delete ℓsentinel m !! ℓtail = Some (ι3, _, ℓnext)⌝%I) as %Eq.
          { iDestruct (auth_node_mapsto_Some with "authM frag") as %Eq.
            destruct (decide (ℓsentinel = ℓtail)) as [->|Neq].
            - (* We derive a contradiction. *)
              iDestruct (node_mapsto_agree with "fragO' frag") as %[-> B].
              iDestruct (state_agree with "nodeState' sentinelState") as %[=].
            - iPureIntro. by apply lookup_delete_Some. }
          (* We lookup the current head of the list/new sentinel. *)
          iDestruct (big_sepM_delete with "bigSep") as "[conjunct bigSep]"; first apply Eq.
          iDestruct "conjunct" as (ιtemp ℓtemp ωtemp [= <- <- <-] x'') "(hdPtPts'' & nn & [[st _]|[[st _]|st]] & disj2)";
          try iDestruct (state_agree with "st nodeState'") as %[=].
          iCombine "nodeState' st" as "nodeState'".
          iMod (update_live_sentinel with "nodeState'") as "[nodeState1 nodeState2]".
          iDestruct (mapsto_agree_frac_frac with "otherPts tailPts") as %[= -> ->].
          iDestruct "Hlink" as "[Hτi Hlink]".
          (* We step through the specificaion code. *)
          iMod (steps_CG_dequeue_cons with "[$Hspec $Hj $Hsq $lofal]") as "(Hj & Hsq & lofal)".
          { solve_ndisj. }
          (* We split qPts again such that we can put one half in the sentinel
             invariant and one half directly in the queue invariant. *)
          iDestruct "qPts" as "[qPts qPts']".
          iMod (update_sentinel_dead with "sentinelState") as "sentinelState".
          (* iDestruct (big_sepM_insert_delete with "[$bigSep]") as "bigSep". *)
          iDestruct (reinsert_node_Some with "bigSep [qPts' nodeState2 nn hdPtPts'' disj2]") as "bigSep".
          { eassumption. }
          { iExistsN. iFrame. iRight. iLeft. iFrame. }
          iDestruct (reinsert_node_Some with "bigSep [sentinelPts sentinelState hdPtPts' tailPts disj]") as "bigSep".
          { apply sentSome. }
          { iDestruct (mapsto_frac_duplicable with "hdPtPts'") as "(hdPtPts1 & hdPtPts2)".
            iDestruct (mapsto_frac_duplicable with "tailPts") as "(tailPts1 & tailPts2)".
            iExistsN. iFrame. 
            iSplitL "hdPtPts1". { iExists _. iRight. iExists _, _, _. iFrame. auto. }
            iLeft. iFrame. iExistsN. iFrame "hdPtPts2". iFrame. }
          iMod ("closeNodesInv" with "[authM bigSep]") as "_".
          { iNext. iExists _. iFrame. }
          iModIntro.
          (* We are now ready to reestablish the invariant. *)
          iMod ("Hclose" with "[qPts lofal Hlink Hsq nodeList nodeState1 tPts lastState tailMapsto]") as "_".
          { iNext. iExists xs2', xsₛ2'. iFrame. iExistsN. iFrame "qPts tPts". iFrame. auto. }
          (* Step over the remainder of the code. *)
          iModIntro. simpl.
          iApply wp_pure_step_later; auto. iNext.
          iApply (wp_bind (fill [CaseCtx _ _; InjRCtx])).
          iApply wp_pure_step_later; auto. iNext.
          iApply wp_value. simpl.
          iApply (wp_bind (fill [InjRCtx])).
          iApply wp_pure_step_later; auto. iNext. simpl.
          iApply wp_value.
          iApply wp_value.
          iExists (InjRV _).
          iFrame.
          iRight.
          iExists (_, _).
          iFrame.
          auto.
        * (* The queue no longer points to the same sentinel. This case should
             be easy with lob induction. *)
          iApply (wp_cas_fail with "qPts"). congruence.
          iNext. iIntros "qPts".
          iDestruct "sentinelState" as "[sentinelState sentinelState']".
          iDestruct "qPts" as "[qPts qPts']".
          iDestruct (reinsert_node_Some with "bigSep [qPts' fragO' next sentinelState sentinelPts' disj]") as "bigSep".
          { eassumption. }
          { iExistsN. iFrame. iRight. iLeft. iFrame. }
          iMod ("closeNodesInv" with "[authM bigSep]") as "_".
          { iNext. iExists _. iFrame. }
          iModIntro.
          iMod ("Hclose" with "[qPts tPts lofal Hsq nodeList sentinelState' Hlink tailMapsto lastState]") as "_".
          { iNext. iExistsN. iFrame. iExistsN. iFrame "qPts tPts". iFrame. auto. }
          iModIntro.
          iApply wp_pure_step_later; auto. iNext.
          iApply ("Hlat" with "[$Hj]").
    - (* enqueue *)
      iIntros ([v1 v2]) "!> #Hrel".
      iIntros (j K) "Hj". simpl.
      rewrite with_lock_of_val.
      iApply wp_pure_step_later; auto. iNext.
      iApply (wp_bind (fill [LetInCtx _])).
      iAsimpl.
      (* Step ove the allocation and creation of the new node. *)
      iApply (wp_bind (fill [PairRCtx (InjRV _); InjRCtx; FoldCtx; AllocCtx])).
      iApply (wp_bind (fill [AllocCtx])).
      iApply wp_alloc; first done.
      iNext. iIntros (ℓnil) "nilPts". simpl.
      iApply wp_alloc; first done.
      iNext. iIntros (ℓtail) "tailPts". simpl.
      iApply wp_alloc; first done.
      iNext. iIntros (ℓnode) "nodePts /=".
      iApply wp_pure_step_later; auto. iNext.
      (* Evaluate the argument to the recursive function. *)
      iApply (wp_bind (fill [AppRCtx (RecV _)])). iAsimpl.
      iApply wp_value. iAsimpl.
      iLöb as "Hlat".
      iApply wp_pure_step_later; auto. iNext. iAsimpl.
      iApply (wp_bind (fill [LetInCtx _])).
      iApply (wp_bind (fill [SndCtx])).
      iApply (wp_bind (fill [CaseCtx _ _])).
      iApply (wp_bind (fill [UnfoldCtx])).
      iApply (wp_bind (fill [LoadCtx])).
      (* Lookup tail *)
      iApply (load_tail with "[$]"). iNext.
      iIntros (ι ω ℓtoTail ℓnext) "#[tailMapsto isLast]".
      iInv nodesN as (m) "[>authM bigSep]" "closeNodesInv".
      iDestruct (node_lookup with "authM bigSep tailMapsto") as "(sentInv & authM & bigSep)".
      iDestruct "sentInv" as (x') "(toTailPts & next & rest)".
      iDestruct (mapsto_frac_duplicable with "toTailPts") as "(toTailPts & toTailPts')".
      iApply (wp_load_frac with "toTailPts").
      iNext. iIntros "toTailPts". simpl.
      iDestruct (reinsert_node with "authM tailMapsto bigSep [toTailPts' next rest]") as "(authM & bigSep)".
      { iExistsFrame. }
      iMod ("closeNodesInv" with "[authM bigSep]") as "_".
      { iNext. iExistsFrame. }
      iModIntro.
      clear m.

      iApply wp_pure_step_later; auto. iNext.
      iApply wp_value.
      iApply wp_pure_step_later; auto. iNext.
      iApply wp_value.
      iApply wp_pure_step_later; auto. iNext.
      iApply wp_value.
      iApply wp_pure_step_later; auto. iNext.
      iApply (wp_bind (fill [LetInCtx _])). iAsimpl.

      iInv nodesN as (m) "[>authM bigSep]" "closeNodesInv".
      iDestruct (node_lookup with "authM bigSep tailMapsto") as "(sentInv & authM & bigSep)".
      iDestruct "sentInv" as (x2) "(toTailPts' & next & rest)".
      iDestruct "next" as (ℓn) "[left | right]".
      + (* We are at the end and can attempt inserting our node. *)
        iDestruct "left" as ">(ℓPts & ℓ2Pts & tok)".
        iDestruct (mapsto_frac_duplicable with "ℓ2Pts") as "(ℓ2Pts & ℓ2Pts')".
        iApply (wp_load with "ℓPts").
        iNext. iIntros "ℓPts". simpl.
        iDestruct (reinsert_node with "authM tailMapsto bigSep [toTailPts' ℓPts ℓ2Pts' tok rest]") as "(authM & bigSep)".
        { iExistsN. iFrame. iExists _. iLeft. iFrame. }
        iMod ("closeNodesInv" with "[authM bigSep]") as "_".
        { iNext. iExists _. iFrame. }
        iModIntro.
        iApply wp_pure_step_later; auto. iNext. iAsimpl.
        iApply (wp_bind (fill [CaseCtx _ _])).
        iApply (wp_bind (fill [UnfoldCtx])).
        simpl.
        iApply (wp_load_frac with "ℓ2Pts").
        iNext. iIntros "ℓ2Pts". simpl.
        iApply wp_pure_step_later; auto; iNext.
        iApply wp_value.
        iApply wp_pure_step_later; auto; iNext. simpl.
        iApply (wp_bind (fill [IfCtx _ _])).
        (* We must open the invariant, case on whether ℓ is equal to ℓ2, and
           extract that ℓ is the last node. *)
        iInv queueN as (xs xsₛ) "(isMSQ & Hsq & lofal & Hlink)" "Hclose".
        rewrite /isMSQueue.
        iDestruct "isMSQ" as (ℓsentinel2 ℓlast ℓhdPt' ℓpt ιT ωT ι' ω')
          "(>qPts & fragO' & >tPts & tailMapsto' & sentState' & >tailState & nodeList)".
        (* Lookup node *)
        clear m.
        iInv nodesN as (m) "[>authM bigSep]" "closeNodesInv".
        iDestruct (node_lookup with "authM bigSep tailMapsto") as "(tailInv & authM & bigSep)".
        iDestruct "tailInv" as (x3) "(>sentinelPts' & next & rest & disj)".
        iDestruct "next" as (ℓn') "next".
        (* We know that we read the nil-node off of ℓn, but someone else might have changed it in the meantime. *)
        destruct (decide (ℓn' = ℓn)) as [|Hneq]; subst.
        * (* We are still at the end. *)
          iAssert (⌜ℓtoTail ≠ ℓnode⌝%I) as %Neq.
          { iIntros (->). iApply (mapsto_exclusive_frac with "nodePts toTailPts"). }
          iDestruct "next" as "[left|right]".
          (* The second case is a contradiction since we know that the node
             points to the same thing as before and we have investigated that node
             and found it to be a nil-node. *)
          2: {
            iDestruct "right" as (?ι ?ω next) "(>hdPts' & >#nextPts)".
            (* We reinsert the node we looked up. This is only done to make the
               next lookup easier. *)
            iDestruct (reinsert_node with "authM tailMapsto bigSep [toTailPts hdPts' rest disj]") as "(>authM & bigSep)".
            { iNext. iExistsFrame. }
            iDestruct (node_lookup with "authM bigSep nextPts") as "(nodeInv & authM & bigSep)".
            iDestruct "nodeInv" as (x4) "(>nPts & nextNode & rest)".
            iDestruct (mapsto_agree_frac_frac with "nPts ℓ2Pts") as %[=]. }
          iDestruct "left" as ">(nextPts & ℓ2Pts2 & tok)".
          simpl.
          iDestruct "disj" as ">[first | [second | third]]".
          3: { iDestruct (state_leq with "third isLast") as %le. inversion le. }
          1: {
            iDestruct "first" as "(_ & H)". iDestruct "H" as (x b ?) "(nextPts' & thenPts)".
            iDestruct (mapsto_agree_frac with "nextPts nextPts'") as %[= <-].
            iDestruct (mapsto_agree_frac_frac with "ℓ2Pts thenPts") as %[=]. }
          iDestruct "second" as "(isLast' & tPts')".
          iDestruct (mapsto_agree with "tPts tPts'") as %[= <-].
          iApply (enqueue_cas with "[$authM $bigSep $nodesInv $nilPts $tailPts $nodePts $nodeList $nextPts $tok]").
          { done. }
          iNext.
          iDestruct 1 as (ι3 ω3) "(authM & bigSep & nodePts & nodeList & #frag & nextPts & tailState')".
          iMod (update_last_no_longer with "isLast'") as "nodeState".
          iMod (steps_CG_enqueue with "[$Hspec $Hj $lofal $Hsq]") as "(Hj & Hsq & lofal)".
          { solve_ndisj. }
          iDestruct (reinsert_node with "authM tailMapsto bigSep [toTailPts nextPts nodePts rest nodeState]") as "(authM & bigSep)".
          { iNext. iExistsN. iFrame.
            iDestruct (mapsto_frac_duplicable with "nextPts") as "(nextPts & nextPts')".
            iSplitL "nextPts". { iExistsN. iRight. iExistsN. iFrame. auto. }
            iExistsFrame. }
          iMod ("closeNodesInv" with "[authM bigSep]").
          { iNext. iExistsFrame. }
          iModIntro.
          iMod ("Hclose" with "[qPts tPts' lofal Hlink Hsq sentinelPts' nodeList sentState' fragO']") as "_".
          { iNext. iExists (xs ++ [v1]), (xsₛ ++ [v2]). iFrame.
            iSplitL. { iExistsN. iFrame. auto. }
            by iApply big_sepL2_singleton. }
          iModIntro.
          iApply wp_pure_step_later; auto; iNext.
          iApply (wp_bind (fill [SeqCtx _])).
          iClear "isLast". clear xs xsₛ ℓhdPt' m x3.
          iInv queueN as (xs xsₛ) "(isMSQ & Hsq & lofal & Hlink)" "Hclose".
          rewrite /isMSQueue.
          iDestruct "isMSQ" as (ℓsentinel ?ℓlast ℓhdPt' ℓpt' ?ι ?ω ι2 ω2)
            "(>qPts & >fragO' & >tPts' & >tailMapsto2 & >sentState' & >tailState2 & nodeList)".
          iDestruct (mapsto_combine with "tPts' tPts") as "[tPts %]".
          rewrite Qp_half_half.
          iInv nodesN as (m) "[>authM bigSep]" "closeNodesInv".
          iDestruct (node_lookup with "authM bigSep frag") as "(tailInv & authM & bigSep)".
          iDestruct "tailInv" as (x3) "(>sentinelPts' & next & rest & disj)".
          iApply (wp_store with "tPts"). iNext.
          iIntros "[tPts tPts']".
          iDestruct "disj" as "[[state _] | [[state _] | state]]";
          try iDestruct (state_agree with "tailState' state") as %[=].
          iCombine "tailState' state" as "state".
          iMod (own_update _ _ (● isLast ⋅ ◯ isLast)  with "state") as "[state fragState]".
          { apply auth_update_alloc. apply mnat_local_update. unfold notYetLast, isLast. lia. }
          iDestruct (reinsert_node with "authM frag bigSep [state sentinelPts' rest next tPts']") as "(authM & bigSep)".
          { iExistsN. iFrame. iRight. iLeft. iFrame. }
          iMod ("closeNodesInv" with "[authM bigSep]").
          { iNext. iExistsFrame. }
          iModIntro.
          iMod ("Hclose" with "[qPts tPts lofal Hlink Hsq nodeList sentState' fragO' fragState]") as "_".
          { iNext. iExistsFrame. }
          iModIntro. simpl.
          iApply wp_pure_step_later; auto; iNext.
          iApply wp_value.
          iExists UnitV. iFrame. done.
        * (* Another thread changed the end in the meantime. *) 
          iDestruct "next" as "[left|right]".
          (* FIXME: There is quite a bit of duplicated code in the two cases below. *)
          -- iDestruct "left" as ">(ℓPts & ℓ2Pts2 & tok)".
            iApply (wp_cas_fail with "ℓPts"). congruence.
            iNext. iIntros "ℓPts".
            iDestruct (reinsert_node with "authM tailMapsto bigSep [sentinelPts' ℓPts ℓ2Pts2 tok rest disj]") as "(authM & bigSep)".
            { iExistsFrame. }
            iMod ("closeNodesInv" with "[authM bigSep]").
            { iNext. iExists _. iFrame. }
            iModIntro.
            iMod ("Hclose" with "[qPts tPts lofal Hlink Hsq nodeList sentState' fragO' tailMapsto' tailState]") as "_".
            { iNext. iExists xs, xsₛ. iFrame. iExistsN. iFrame "qPts". iFrame. }
            iModIntro.
            simpl.
            iApply wp_pure_step_later; auto; iNext.
            iApply ("Hlat" with "Hj nilPts tailPts nodePts").
          -- iDestruct "right" as (?ι x4 next) "(>ℓPts & nodeInvNext)".
            iApply (wp_cas_fail_frac with "ℓPts"). congruence.
            iNext. iIntros "ℓPts".
            iDestruct (reinsert_node with "authM tailMapsto bigSep [sentinelPts' ℓPts nodeInvNext rest disj]") as "(authM & bigSep)".
            { iExistsFrame. }
            iMod ("closeNodesInv" with "[authM bigSep]").
            { iNext. iExists _. iFrame. }
            iModIntro.
            iMod ("Hclose" with "[qPts tPts lofal Hlink Hsq nodeList sentState' fragO' tailMapsto' tailState]") as "_".
            { iNext. iExists xs, xsₛ. iFrame. iExistsN. iFrame "qPts". iFrame. }
            iModIntro.
            simpl.
            iApply wp_pure_step_later; auto; iNext.
            iApply ("Hlat" with "Hj nilPts tailPts nodePts").
      + (* We are not at the end yet, keep on going. *)
        iDestruct "right" as (?ι ?ω next) "(>ℓPts & #nodeInvNext)".
        iApply (wp_load_frac with "ℓPts").
        iNext. iIntros "ℓPts". simpl.
        iDestruct (reinsert_node with "authM tailMapsto bigSep [ℓPts rest nodeInvNext toTailPts]") as "(authM & bigSep)".
        { iExistsN. iFrame. iExistsN. iRight. iExistsFrame. }
        iMod ("closeNodesInv" with "[authM bigSep]").
        { iNext. iExists _. iFrame. }
        iModIntro.
        iApply wp_pure_step_later; auto. iNext.
        iApply (wp_bind (fill [CaseCtx _ _])).
        iApply (wp_bind (fill [UnfoldCtx])). iAsimpl.
        clear m.
        iInv nodesN as (m) "[>authM bigSep]" "closeNodesInv".
        iDestruct (node_lookup with "authM bigSep nodeInvNext") as "(nodeInv & authM & bigSep)".
        iDestruct "nodeInv" as (x4) "(nPts & nextNode & rest)".
        iApply (wp_load_frac with "nPts").
        iNext. iIntros "nPts". simpl.
        iDestruct (reinsert_node with "authM nodeInvNext bigSep [nPts nextNode rest]") as "(authM & bigSep)".
        { iExistsFrame. }
        iMod ("closeNodesInv" with "[authM bigSep]") as "_".
        { iNext. iExists _. iFrame. }
        iModIntro.
        iApply wp_pure_step_later; auto; iNext.
        iApply wp_value.
        iApply wp_pure_step_later; auto; iNext.
        iApply (wp_bind (fill [AppRCtx (RecV _)])).
        simpl.
        iApply wp_value. simpl.
        iApply ("Hlat" with "Hj nilPts tailPts nodePts").
  Qed.

End Queue_refinement.
